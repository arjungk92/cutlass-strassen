#include <cstdio>
#include <cuda_runtime.h>
#include "cuda.h"
#include "cublas_v2.h"
#include <cmath>

#define Ms 64
#define Ns 64
#define Ks 8
#define Mr 8
#define Nr 8

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }

inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
    if (code != cudaSuccess)
    {
        fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
        if (abort) exit(code);
    }
}

//TODO: Make me dynamic.
int localNMul = 256;
int localNAdd = 256;

bool checkMatrix(const double *, const double *, int);
void printMat(double *, unsigned int, int, int, char *);
float global_strassen_wrapper(double *, double *, double *, double *, unsigned int, unsigned int, bool, bool, bool);
float local_strassen_wrapper(double *, double *, double *, double *, unsigned int, unsigned int, bool, bool, bool);
bool validate(const double *, const double *, const double *, int);

__global__ void matrix_gen(double *a, int N, bool is_zero) {
    unsigned int gid = blockDim.x * blockIdx.x + threadIdx.x;

    if(gid < N * N) {
        if(is_zero)
            a[gid] = 0;
        else
            a[gid] = gid % 32;
    }

}

__global__ void cublas_cutlass(const double *a, const double *b, double *c, unsigned int N) {
    //Block offsets.
    unsigned int block_offset = N / gridDim.x;
    unsigned int x_block_offset = block_offset * blockIdx.x;
    unsigned int y_block_offset = block_offset * blockIdx.y;
    unsigned int thread_offset_long = Ms / blockDim.x;
    unsigned int thread_offset_short = Ks / blockDim.x;
    //Thread offsets.
    unsigned int x_thread_offset_a = threadIdx.x * thread_offset_long;
    unsigned int y_thread_offset_a = threadIdx.y * thread_offset_short;
    unsigned int x_thread_offset_b = threadIdx.x * thread_offset_short;
    unsigned int y_thread_offset_b = threadIdx.y * thread_offset_long;

    __shared__ double as[Ms][Ks];
    __shared__ double bs[Ks][Ns];

    //Load to shared memory.

    int block_k = 0;
    #pragma unroll
    for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
        unsigned int m_a = i / thread_offset_short;
        unsigned int k_a = i % thread_offset_short;
        unsigned int k_b = i / thread_offset_long;
        unsigned int n_b = i % thread_offset_long;
        as[x_thread_offset_a + m_a][y_thread_offset_a + k_a] = a[(x_block_offset + x_thread_offset_a + m_a) * N +
                                                                 (block_k * Ks) + (y_thread_offset_a + k_a)];
        bs[x_thread_offset_b + k_b][y_thread_offset_b + n_b] = b[(x_thread_offset_b + k_b + (block_k * Ks)) * N +
                                                                 (y_block_offset + y_thread_offset_b + n_b)];
    }

    //--------------------BARRIER---------------------
    __syncthreads();


    //Define registers.
    double c_accumulator[Mr][Nr] = {0}; //Holds result of mul. //static storage duration.
    double a_frag[2][Mr]; //While one vector of length Mr is being processed the other Mr vector prefetches.
    double b_frag[2][Nr]; // ,,
    double a_next[Mr]; //Prefetch for the next shared memory loading.
    double b_next[Nr]; // ,,

    for(block_k = 0; block_k < N / Ks; block_k++) {
        if(block_k + 1 < N / Ks) {
            #pragma unroll
            for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                unsigned int m_a = i / thread_offset_short;
                unsigned int k_a = i % thread_offset_short;
                unsigned int k_b = i / thread_offset_long;
                unsigned int n_b = i % thread_offset_long;
                a_next[i] = a[(x_block_offset + x_thread_offset_a + m_a) * N + ((block_k + 1) * Ks) +
                              (y_thread_offset_a + k_a)];
                b_next[i] = b[(x_thread_offset_b + k_b + ((block_k + 1) * Ks)) * N +
                              (y_block_offset + y_thread_offset_b + n_b)];
            }
        }

        //Load to registers.
        int warp_k = 0;
        #pragma unroll
        for(int i = 0; i < Mr; i++) { //Can be done since Mr == Nr // When Mr != Nr separate b_frag load to another loop.
            a_frag[0][i] = as[threadIdx.x * Mr + i][warp_k];
            b_frag[0][i] = bs[warp_k][threadIdx.y * Nr + i];
        }
        for(warp_k = 0; warp_k < Ks; warp_k++) {
            if(warp_k + 1 < Ks) {
                #pragma unroll
                for (int i = 0; i < Mr; i++) { //Can be done since Mr == Nr
                    a_frag[(warp_k + 1) % 2][i] = as[threadIdx.x * Mr + i][warp_k + 1];
                    b_frag[(warp_k + 1) % 2][i] = bs[warp_k + 1][threadIdx.y * Nr + i];
                }
            }


            #pragma unroll
            for(int i = 0; i < Mr / 4; i++) {
                #pragma unroll
                for(int j = 0; j < Nr; j++) {
                    c_accumulator[i * 4][j] += a_frag[warp_k % 2][i * 4] * b_frag[warp_k % 2][j];
                    c_accumulator[i * 4 + 1][j] += a_frag[warp_k % 2][i * 4 + 1] * b_frag[warp_k % 2][j];
                    c_accumulator[i * 4 + 2][j] += a_frag[warp_k % 2][i * 4 + 2] * b_frag[warp_k % 2][j];
                    c_accumulator[i * 4 + 3][j] += a_frag[warp_k % 2][i * 4 + 3] * b_frag[warp_k % 2][j];
                }
            }
        }

        //--------------------BARRIER---------------------
        __syncthreads();

        if(block_k + 1 < N / Ks) {
            #pragma unroll
            for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                unsigned int m_a = i / thread_offset_short;
                unsigned int k_a = i % thread_offset_short;
                unsigned int k_b = i / thread_offset_long;
                unsigned int n_b = i % thread_offset_long;
                as[x_thread_offset_a + m_a][y_thread_offset_a + k_a] = a_next[i];
                bs[x_thread_offset_b + k_b][y_thread_offset_b + n_b] = b_next[i];
            }
        }

        //--------------------BARRIER---------------------
        __syncthreads();
    }

    #pragma unroll
    for(int i = 0; i < Mr; i++) {
        #pragma unroll
        for (int j = 0; j < Nr; j++) {
            c[(i + (threadIdx.x + blockIdx.x * blockDim.x) * Mr) * N + (j + (threadIdx.y + blockIdx.y * blockDim.y) * Nr)] = c_accumulator[i][j];
        }
    }
}

__global__ void strassen_cutlass_stage1(double *a, double *b, unsigned int N) {
    unsigned int gid_x = threadIdx.x + blockIdx.x * blockDim.x;
    unsigned int gid_y = threadIdx.y + blockIdx.y * blockDim.y;
    unsigned int tot_x = gridDim.x * blockDim.x;
    unsigned int tot_y = gridDim.y * blockDim.y;
    unsigned int thread_offset_long = Ms / blockDim.y;
    unsigned int thread_offset_short = Ks / blockDim.x;

    unsigned int zero_start = 0;
    unsigned int one_start = N / 2;
    unsigned int two_start = (N * N) / 2;
    unsigned int three_start = (N * N + N) / 2;

    __shared__ double as[Ms][Ks];
    __shared__ double bs[Ks][Ns];

    //Define registers.
    double a_frag[2][Mr]; //While one vector of length Mr is being processed the other Mr vector prefetches.

    double *add_source;
    unsigned int start_1, start_2, source_start;
    bool is_add;
    unsigned int block_local_x, block_local_y, block_offset, thread_offset;
    if(gid_x < tot_x / 2 && gid_y < tot_y / 2) {
        //Group 1
        block_local_x = blockIdx.x;
        block_local_y = blockIdx.y;

        //Addition. a0 + a1 -> a1
        add_source = a;
        start_1 = zero_start;
        start_2 = one_start;
        source_start = one_start;
        is_add = true;
    }
    else if(gid_x >= tot_x / 2 && gid_y < tot_y / 2) {
        //Group 2
        block_local_x = blockIdx.x - gridDim.x / 2;
        block_local_y = blockIdx.y;

        //Subtraction. b1 - b3 -> b1
        add_source = b;
        start_1 = one_start;
        start_2 = three_start;
        source_start = one_start;
        is_add = false;
    }
    else if(gid_x < tot_x / 2 && gid_y >= tot_y / 2) {
        //Group 3
        block_local_x = blockIdx.x;
        block_local_y = blockIdx.y - gridDim.y / 2;

        //Subtraction. b2 - b0 -> b2
        add_source = b;
        start_1 = two_start;
        start_2 = zero_start;
        source_start = two_start;
        is_add = false;
    }
    else if(gid_x >= tot_x / 2 && gid_y >= tot_y / 2) {
        //Group 4
        block_local_x = blockIdx.x - gridDim.x / 2;
        block_local_y = blockIdx.y - gridDim.y / 2;

        //Addition. a2 + a3 -> a2
        add_source = a;
        start_1 = two_start;
        start_2 = three_start;
        source_start = two_start;
        is_add = true;
    }
    else {
        printf("gid_x = %d, gid_y = %d out of bounds\n", gid_x, gid_y);
    }

    block_offset = block_local_y * N * Ms + block_local_x * Ns;
    thread_offset = threadIdx.y * thread_offset_long * N + threadIdx.x * thread_offset_short;

    int block_k = 0;
    int iteration_offset = block_k * Ks;
    #pragma unroll
    for(int i = 0; i < thread_offset_long * thread_offset_short; i++) {
        as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] =
                add_source[start_1 + block_offset + iteration_offset +thread_offset + i * N];
        bs[threadIdx.x * thread_offset_short][threadIdx.y * thread_offset_long + i] =
                add_source[start_2 + block_offset + iteration_offset + thread_offset + i * N];
    }

    if(is_add) {
        #pragma unroll
        for (block_k = 0; block_k < Ns / Ks; block_k++) {
            iteration_offset = (block_k + 1) * Ks;
            #pragma unroll
            for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                a_frag[0][i] = as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] +
                               bs[threadIdx.x * thread_offset_short][threadIdx.y * thread_offset_long + i];

                if (block_k + 1 < Ns / Ks) {
                    as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] =
                            add_source[start_1 + block_offset + iteration_offset + thread_offset + i * N];
                    bs[threadIdx.x * thread_offset_short][threadIdx.y * thread_offset_long + i] =
                            add_source[start_2 + block_offset + iteration_offset + thread_offset + i * N];
                }
            }

            //Push values back to a
            iteration_offset = block_k * Ks;
            #pragma unroll
            for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                add_source[source_start + block_offset + iteration_offset + thread_offset + i * N] = a_frag[0][i];
            }
        }
    }
    else {
        #pragma unroll
        for (block_k = 0; block_k < Ns / Ks; block_k++) {
            iteration_offset = (block_k + 1) * Ks;
            #pragma unroll
            for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                a_frag[0][i] = as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] -
                               bs[threadIdx.x * thread_offset_short][threadIdx.y * thread_offset_long + i];

                if (block_k + 1 < Ns / Ks) {
                    as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] =
                            add_source[start_1 + block_offset + iteration_offset + thread_offset + i * N];
                    bs[threadIdx.x * thread_offset_short][threadIdx.y * thread_offset_long + i] =
                            add_source[start_2 + block_offset + iteration_offset + thread_offset + i * N];
                }
            }

            //Push values back to b
            iteration_offset = block_k * Ks;
            #pragma unroll
            for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                add_source[source_start + block_offset + iteration_offset + thread_offset + i * N] = a_frag[0][i];
            }
        }
    }
}

__global__ void strassen_cutlass_stage2(const double *a, const double *b, double *c, unsigned int N) {
    unsigned int gid_x = threadIdx.x + blockIdx.x * blockDim.x;
    unsigned int gid_y = threadIdx.y + blockIdx.y * blockDim.y;
    unsigned int tot_x = gridDim.x * blockDim.x;
    unsigned int tot_y = gridDim.y * blockDim.y;
    unsigned int thread_offset_long = Ms / blockDim.y;
    unsigned int thread_offset_short = Ks / blockDim.x;

    unsigned int zero_start = 0;
    unsigned int one_start = N / 2;
    unsigned int two_start = (N * N) / 2;
    unsigned int three_start = (N * N + N) / 2;

    __shared__ double as[Ms][Ks];
    __shared__ double bs[Ks][Ns];

    //Define registers.
    double c_accumulator[Mr][Nr] = {0}; //Holds result of mul. //static storage duration.
    double a_frag[2][Mr]; //While one vector of length Mr is being processed the other Mr vector prefetches.
    double b_frag[2][Nr]; // ,,
    double a_next[Mr]; //Prefetch for the next shared memory loading.
    double b_next[Nr]; // ,,

    unsigned int start_a, start_b, source_start_1, source_start_2;
    unsigned int block_local_x, block_local_y, block_offset_a , block_offset_b,
    thread_offset_a, thread_offset_b, thread_offset_c;
    bool is_dest_add_1 = true, is_dest_add_2 = true;

    if(gid_x < tot_x / 2 && gid_y < tot_y / 2) {
        //Group 1
        block_local_x = blockIdx.x;
        block_local_y = blockIdx.y;

        //Mul a2 * b0 -> c2, c3
        start_a = two_start;
        start_b = zero_start;
        source_start_1 = two_start;
        source_start_2 = three_start;
        is_dest_add_2 = false;
    }
    else if(gid_x >= tot_x / 2 && gid_y < tot_y / 2) {
        //Group 2
        block_local_x = blockIdx.x - gridDim.x / 2;
        block_local_y = blockIdx.y;

        //Mul a0 * b1 -> c1, c3
        start_a = zero_start;
        start_b = one_start;
        source_start_1 = one_start;
        source_start_2 = three_start;
    }
    else if(gid_x < tot_x / 2 && gid_y >= tot_y / 2) {
        //Group 3
        block_local_x = blockIdx.x;
        block_local_y = blockIdx.y - gridDim.y / 2;

        //Mul a1 * b3 -> c1, c0
        start_a = one_start;
        start_b = three_start;
        source_start_1 = one_start;
        source_start_2 = zero_start;
        is_dest_add_2 = false;
    }
    else if(gid_x >= tot_x / 2 && gid_y >= tot_y / 2) {
        //Group 4
        block_local_x = blockIdx.x - gridDim.x / 2;
        block_local_y = blockIdx.y - gridDim.y / 2;

        //Mul a3 * b2 -> c0, c2
        start_a = three_start;
        start_b = two_start;
        source_start_1 = zero_start;
        source_start_2 = two_start;
    }
    else {
        printf("gid_x = %d, gid_y = %d out of bounds\n", gid_x, gid_y);
    }

    block_offset_a = block_local_y * N * Ms;
    block_offset_b = block_local_x * Ns;
    thread_offset_a = threadIdx.y * thread_offset_long * N + threadIdx.x * thread_offset_short;
    thread_offset_b = threadIdx.y * thread_offset_short * N + threadIdx.x * thread_offset_long;
    thread_offset_c = threadIdx.y * Mr * N + threadIdx.x * Nr;

    int block_k = 0, warp_k = 0;
    unsigned int iteration_offset_a = block_k * Ks;
    unsigned int iteration_offset_b = block_k * Ks * N;

    #pragma unroll
    for(int i = 0; i < thread_offset_long * thread_offset_short; i++) {
        as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] =
                a[start_a + block_offset_a + iteration_offset_a + thread_offset_a + i * N];
        bs[threadIdx.y * thread_offset_short][threadIdx.x * thread_offset_long + i] =
                b[start_b + block_offset_b + iteration_offset_b + thread_offset_b + i];
    }
    __syncthreads();

    #pragma unroll
    for (block_k = 0; block_k < N /(2 * Ks); block_k++) {
        if((block_k + 1) < N /(2 * Ks)) {
            iteration_offset_a = (block_k + 1) * Ks;
            iteration_offset_b = (block_k + 1) * Ks * N;
            #pragma unroll
            for(int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                a_next[i] = a[start_a + block_offset_a + iteration_offset_a + thread_offset_a + i * N];
                b_next[i] = b[start_b + block_offset_b + iteration_offset_b + thread_offset_b + i];
            }
        }

        //Load to registers.
        warp_k = 0;
        #pragma unroll
        for(int i = 0; i < Mr; i++) { //Can be done since Mr == Nr // When Mr != Nr separate b_frag load to another loop.
            a_frag[0][i] = as[threadIdx.y * Mr + i][warp_k];
            b_frag[0][i] = bs[warp_k][threadIdx.x * Nr + i];
        }

        for(warp_k = 0; warp_k < Ks; warp_k++) {
            if(warp_k + 1 < Ks) {
                #pragma unroll
                for (int i = 0; i < Mr; i++) { //Can be done since Mr == Nr
                    a_frag[(warp_k + 1) % 2][i] = as[threadIdx.y * Mr + i][warp_k + 1];
                    b_frag[(warp_k + 1) % 2][i] = bs[warp_k + 1][threadIdx.x * Nr + i];
                }
            }

            #pragma unroll
            for(int i = 0; i < Mr / 4; i++) {
                #pragma unroll
                for(int j = 0; j < Nr; j++) {
                    c_accumulator[i * 4][j] += a_frag[warp_k % 2][i * 4] * b_frag[warp_k % 2][j];
                    c_accumulator[i * 4 + 1][j] += a_frag[warp_k % 2][i * 4 + 1] * b_frag[warp_k % 2][j];
                    c_accumulator[i * 4 + 2][j] += a_frag[warp_k % 2][i * 4 + 2] * b_frag[warp_k % 2][j];
                    c_accumulator[i * 4 + 3][j] += a_frag[warp_k % 2][i * 4 + 3] * b_frag[warp_k % 2][j];
                }
            }
        }

        //--------------------BARRIER---------------------
        __syncthreads();

        if(block_k + 1 < N / (2 * Ks)) {
            #pragma unroll
            for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] = a_next[i];
                bs[threadIdx.y * thread_offset_short][threadIdx.x * thread_offset_long + i] = b_next[i];
            }
        }

        //--------------------BARRIER---------------------
        __syncthreads();
    }

    #pragma unroll
    for(int i = 0; i < Mr; i++) {
        #pragma unroll
        for (int j = 0; j < Nr; j++) {
            if(is_dest_add_1) {
                atomicAdd(c + source_start_1 + block_offset_a + block_offset_b + thread_offset_c + i * N + j,
                          c_accumulator[i][j]
                );
            }
            else {
                atomicAdd(c + source_start_1 + block_offset_a + block_offset_b + thread_offset_c + i * N + j,
                         -1 * c_accumulator[i][j]
                );
            }

            if(is_dest_add_2) {
                atomicAdd(c + source_start_2 + block_offset_a + block_offset_b + thread_offset_c + i * N + j,
                          c_accumulator[i][j]
                );
            }
            else {
                atomicAdd(c + source_start_2 + block_offset_a + block_offset_b + thread_offset_c + i * N + j,
                          -1 * c_accumulator[i][j]
                );
            }

        }
    }
}

__global__ void strassen_cutlass_stage3(double *a, double *b, unsigned int N) {
    unsigned int gid_x = threadIdx.x + blockIdx.x * blockDim.x;
    unsigned int gid_y = threadIdx.y + blockIdx.y * blockDim.y;
    unsigned int tot_x = gridDim.x * blockDim.x;
    unsigned int tot_y = gridDim.y * blockDim.y;
    unsigned int thread_offset_long = Ms / blockDim.y;
    unsigned int thread_offset_short = Ks / blockDim.x;

    unsigned int zero_start = 0;
    unsigned int one_start = N / 2;
    unsigned int two_start = (N * N) / 2;
    unsigned int three_start = (N * N + N) / 2;

    __shared__ double as[Ms][Ks];
    __shared__ double bs[Ks][Ns];

    //Define registers.
    double a_frag[2][Mr]; //While one vector of length Mr is being processed the other Mr vector prefetches.

    double *add_source;
    unsigned int start_1, start_2, source_start;
    bool is_add, is_active = false;
    unsigned int block_local_x, block_local_y, block_offset, thread_offset;
    if(gid_x < tot_x / 2 && gid_y < tot_y / 2) {
        //Group 1
        block_local_x = blockIdx.x;
        block_local_y = blockIdx.y;

        //Addition. a0 + a3 -> a0
        add_source = a;
        start_1 = zero_start;
        start_2 = three_start;
        source_start = zero_start;
        is_add = true;
        is_active = true;
    }
    else if(gid_x >= tot_x / 2 && gid_y < tot_y / 2) {
        //Group 2
        block_local_x = blockIdx.x - gridDim.x / 2;
        block_local_y = blockIdx.y;

        //Subtraction. b1 - b3 -> b1
//        add_source = b;
//        start_1 = one_start;
//        start_2 = three_start;
//        source_start = one_start;
        is_add = false;
    }
    else if(gid_x < tot_x / 2 && gid_y >= tot_y / 2) {
        //Group 3
        block_local_x = blockIdx.x;
        block_local_y = blockIdx.y - gridDim.y / 2;

        //Subtraction. b2 - b0 -> b2
//        add_source = b;
//        start_1 = two_start;
//        start_2 = zero_start;
//        source_start = two_start;
        is_add = false;
    }
    else if(gid_x >= tot_x / 2 && gid_y >= tot_y / 2) {
        //Group 4
        block_local_x = blockIdx.x - gridDim.x / 2;
        block_local_y = blockIdx.y - gridDim.y / 2;

        //Addition. b0 + b3 -> b0
        add_source = b;
        start_1 = zero_start;
        start_2 = three_start;
        source_start = zero_start;
        is_add = true;
        is_active = true;
    }
    else {
        printf("gid_x = %d, gid_y = %d out of bounds\n", gid_x, gid_y);
    }

    block_offset = block_local_y * N * Ms + block_local_x * Ns;
    thread_offset = threadIdx.y * thread_offset_long * N + threadIdx.x * thread_offset_short;

    int block_k = 0;
    int iteration_offset = block_k * Ks;
    #pragma unroll
    for(int i = 0; i < thread_offset_long * thread_offset_short; i++) {
        as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] =
                add_source[start_1 + block_offset + iteration_offset +thread_offset + i * N];
        bs[threadIdx.x * thread_offset_short][threadIdx.y * thread_offset_long + i] =
                add_source[start_2 + block_offset + iteration_offset + thread_offset + i * N];
    }

    if(is_add && is_active) {
        #pragma unroll
        for (block_k = 0; block_k < Ns / Ks; block_k++) {
            iteration_offset = (block_k + 1) * Ks;
            #pragma unroll
            for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                a_frag[0][i] = as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] +
                               bs[threadIdx.x * thread_offset_short][threadIdx.y * thread_offset_long + i];

                if (block_k + 1 < Ns / Ks) {
                    as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] =
                            add_source[start_1 + block_offset + iteration_offset + thread_offset + i * N];
                    bs[threadIdx.x * thread_offset_short][threadIdx.y * thread_offset_long + i] =
                            add_source[start_2 + block_offset + iteration_offset + thread_offset + i * N];
                }
            }

            //Push values back to a
            iteration_offset = block_k * Ks;
            #pragma unroll
            for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                add_source[source_start + block_offset + iteration_offset + thread_offset + i * N] = a_frag[0][i];
            }
        }
    }
    else if(is_active){
        #pragma unroll
        for (block_k = 0; block_k < Ns / Ks; block_k++) {
            iteration_offset = (block_k + 1) * Ks;
            #pragma unroll
            for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                a_frag[0][i] = as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] -
                               bs[threadIdx.x * thread_offset_short][threadIdx.y * thread_offset_long + i];

                if (block_k + 1 < Ns / Ks) {
                    as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] =
                            add_source[start_1 + block_offset + iteration_offset + thread_offset + i * N];
                    bs[threadIdx.x * thread_offset_short][threadIdx.y * thread_offset_long + i] =
                            add_source[start_2 + block_offset + iteration_offset + thread_offset + i * N];
                }
            }

            //Push values back to b
            iteration_offset = block_k * Ks;
            #pragma unroll
            for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                add_source[source_start + block_offset + iteration_offset + thread_offset + i * N] = a_frag[0][i];
            }
        }
    }
}

__global__ void strassen_cutlass_stage4(double *a, double *b, unsigned int N) {
    unsigned int gid_x = threadIdx.x + blockIdx.x * blockDim.x;
    unsigned int gid_y = threadIdx.y + blockIdx.y * blockDim.y;
    unsigned int tot_x = gridDim.x * blockDim.x;
    unsigned int tot_y = gridDim.y * blockDim.y;
    unsigned int thread_offset_long = Ms / blockDim.y;
    unsigned int thread_offset_short = Ks / blockDim.x;

    unsigned int zero_start = 0;
    unsigned int one_start = N / 2;
    unsigned int two_start = (N * N) / 2;
    unsigned int three_start = (N * N + N) / 2;

    __shared__ double as[Ms][Ks];
    __shared__ double bs[Ks][Ns];

    //Define registers.
    double a_frag[2][Mr]; //While one vector of length Mr is being processed the other Mr vector prefetches.

    double *add_source;
    unsigned int start_1, start_2, source_start;
    bool is_add;
    unsigned int block_local_x, block_local_y, block_offset, thread_offset;
    if(gid_x < tot_x / 2 && gid_y < tot_y / 2) {
        //Group 1
        block_local_x = blockIdx.x;
        block_local_y = blockIdx.y;

        //Addition. a2 - a0 -> a2
        add_source = a;
        start_1 = two_start;
        start_2 = zero_start;
        source_start = two_start;
        is_add = false;
    }
    else if(gid_x >= tot_x / 2 && gid_y < tot_y / 2) {
        //Group 2
        block_local_x = blockIdx.x - gridDim.x / 2;
        block_local_y = blockIdx.y;

        //Subtraction. a1 - a0 -> a1
        add_source = a;
        start_1 = one_start;
        start_2 = zero_start;
        source_start = one_start;
        is_add = false;
    }
    else if(gid_x < tot_x / 2 && gid_y >= tot_y / 2) {
        //Group 3
        block_local_x = blockIdx.x;
        block_local_y = blockIdx.y - gridDim.y / 2;

        //Subtraction. b1 + b0 -> b1
        add_source = b;
        start_1 = one_start;
        start_2 = zero_start;
        source_start = one_start;
        is_add = true;
    }
    else if(gid_x >= tot_x / 2 && gid_y >= tot_y / 2) {
        //Group 4
        block_local_x = blockIdx.x - gridDim.x / 2;
        block_local_y = blockIdx.y - gridDim.y / 2;

        //Addition. b2 + b0 -> b2
        add_source = b;
        start_1 = two_start;
        start_2 = zero_start;
        source_start = two_start;
        is_add = true;
    }
    else {
        printf("gid_x = %d, gid_y = %d out of bounds\n", gid_x, gid_y);
    }

    block_offset = block_local_y * N * Ms + block_local_x * Ns;
    thread_offset = threadIdx.y * thread_offset_long * N + threadIdx.x * thread_offset_short;

    int block_k = 0;
    int iteration_offset = block_k * Ks;
    #pragma unroll
    for(int i = 0; i < thread_offset_long * thread_offset_short; i++) {
        as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] =
                add_source[start_1 + block_offset + iteration_offset +thread_offset + i * N];
        bs[threadIdx.x * thread_offset_short][threadIdx.y * thread_offset_long + i] =
                add_source[start_2 + block_offset + iteration_offset + thread_offset + i * N];
    }

    if(is_add) {
        #pragma unroll
        for (block_k = 0; block_k < Ns / Ks; block_k++) {
            iteration_offset = (block_k + 1) * Ks;
            #pragma unroll
            for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                a_frag[0][i] = as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] +
                               bs[threadIdx.x * thread_offset_short][threadIdx.y * thread_offset_long + i];

                if (block_k + 1 < Ns / Ks) {
                    as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] =
                            add_source[start_1 + block_offset + iteration_offset + thread_offset + i * N];
                    bs[threadIdx.x * thread_offset_short][threadIdx.y * thread_offset_long + i] =
                            add_source[start_2 + block_offset + iteration_offset + thread_offset + i * N];
                }
            }

            //Push values back to a
            iteration_offset = block_k * Ks;
            #pragma unroll
            for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                add_source[source_start + block_offset + iteration_offset + thread_offset + i * N] = a_frag[0][i];
            }
        }
    }
    else {
        #pragma unroll
        for (block_k = 0; block_k < Ns / Ks; block_k++) {
            iteration_offset = (block_k + 1) * Ks;
            #pragma unroll
            for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                a_frag[0][i] = as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] -
                               bs[threadIdx.x * thread_offset_short][threadIdx.y * thread_offset_long + i];

                if (block_k + 1 < Ns / Ks) {
                    as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] =
                            add_source[start_1 + block_offset + iteration_offset + thread_offset + i * N];
                    bs[threadIdx.x * thread_offset_short][threadIdx.y * thread_offset_long + i] =
                            add_source[start_2 + block_offset + iteration_offset + thread_offset + i * N];
                }
            }

            //Push values back to b
            iteration_offset = block_k * Ks;
            #pragma unroll
            for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                add_source[source_start + block_offset + iteration_offset + thread_offset + i * N] = a_frag[0][i];
            }
        }
    }
}

__global__ void strassen_cutlass_stage5(const double *a, const double *b, double *c, unsigned int N) {
    unsigned int gid_x = threadIdx.x + blockIdx.x * blockDim.x;
    unsigned int gid_y = threadIdx.y + blockIdx.y * blockDim.y;
    unsigned int tot_x = gridDim.x * blockDim.x;
    unsigned int tot_y = gridDim.y * blockDim.y;
    unsigned int thread_offset_long = Ms / blockDim.y;
    unsigned int thread_offset_short = Ks / blockDim.x;

    unsigned int zero_start = 0;
    unsigned int one_start = N / 2;
    unsigned int two_start = (N * N) / 2;
    unsigned int three_start = (N * N + N) / 2;

    __shared__ double as[Ms][Ks];
    __shared__ double bs[Ks][Ns];

    //Define registers.
    double c_accumulator[Mr][Nr] = {0}; //Holds result of mul. //static storage duration.
    double a_frag[2][Mr]; //While one vector of length Mr is being processed the other Mr vector prefetches.
    double b_frag[2][Nr]; // ,,
    double a_next[Mr]; //Prefetch for the next shared memory loading.
    double b_next[Nr]; // ,,

    unsigned int start_a, start_b, source_start_1, source_start_2;
    unsigned int block_local_x, block_local_y, block_offset_a , block_offset_b,
            thread_offset_a, thread_offset_b, thread_offset_c;
    bool is_dual = false, is_active = false;

    if(gid_x < tot_x / 2 && gid_y < tot_y / 2) {
        //Group 1
        block_local_x = blockIdx.x;
        block_local_y = blockIdx.y;

        //Mul a0 * b0 -> c0, c3
        start_a = zero_start;
        start_b = zero_start;
        source_start_1 = zero_start;
        source_start_2 = three_start;
        is_active = true;
        is_dual = true;
    }
    else if(gid_x >= tot_x / 2 && gid_y < tot_y / 2) {
        //Group 2
        block_local_x = blockIdx.x - gridDim.x / 2;
        block_local_y = blockIdx.y;

        //Mul a0 * b1 -> c1, c3
//        start_a = zero_start;
//        start_b = one_start;
//        source_start_1 = one_start;
//        source_start_2 = three_start;
        is_active = false;
    }
    else if(gid_x < tot_x / 2 && gid_y >= tot_y / 2) {
        //Group 3
        block_local_x = blockIdx.x;
        block_local_y = blockIdx.y - gridDim.y / 2;

        //Mul a2 * b1 -> c3
        start_a = two_start;
        start_b = one_start;
        source_start_1 = three_start;
//        source_start_2 = zero_start;
        is_active = true;
    }
    else if(gid_x >= tot_x / 2 && gid_y >= tot_y / 2) {
        //Group 4
        block_local_x = blockIdx.x - gridDim.x / 2;
        block_local_y = blockIdx.y - gridDim.y / 2;

        //Mul a1 * b2 -> c0
        start_a = one_start;
        start_b = two_start;
        source_start_1 = zero_start;
//        source_start_2 = two_start;
        is_active = true;
    }
    else {
        printf("gid_x = %d, gid_y = %d out of bounds\n", gid_x, gid_y);
    }

    if(is_active) {
        block_offset_a = block_local_y * N * Ms;
        block_offset_b = block_local_x * Ns;
        thread_offset_a = threadIdx.y * thread_offset_long * N + threadIdx.x * thread_offset_short;
        thread_offset_b = threadIdx.y * thread_offset_short * N + threadIdx.x * thread_offset_long;
        thread_offset_c = threadIdx.y * Mr * N + threadIdx.x * Nr;

        int block_k = 0, warp_k = 0;
        unsigned int iteration_offset_a = block_k * Ks;
        unsigned int iteration_offset_b = block_k * Ks * N;
        #pragma unroll
        for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
            as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] =
                    a[start_a + block_offset_a + iteration_offset_a + thread_offset_a + i * N];
            bs[threadIdx.y * thread_offset_short][threadIdx.x * thread_offset_long + i] =
                    b[start_b + block_offset_b + iteration_offset_b + thread_offset_b + i];
        }
        __syncthreads();

        #pragma unroll
        for (block_k = 0; block_k < N / (2 * Ks); block_k++) {
            if ((block_k + 1) < N / (2 * Ks)) {
                iteration_offset_a = (block_k + 1) * Ks;
                iteration_offset_b = (block_k + 1) * Ks * N;
                #pragma unroll
                for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                    a_next[i] = a[start_a + block_offset_a + iteration_offset_a + thread_offset_a + i * N];
                    b_next[i] = b[start_b + block_offset_b + iteration_offset_b + thread_offset_b + i];
                }
            }

            //Load to registers.
            warp_k = 0;
            #pragma unroll
            for (int i = 0;
                 i < Mr; i++) { //Can be done since Mr == Nr // When Mr != Nr separate b_frag load to another loop.
                a_frag[0][i] = as[threadIdx.y * Mr + i][warp_k];
                b_frag[0][i] = bs[warp_k][threadIdx.x * Nr + i];
            }

            for (warp_k = 0; warp_k < Ks; warp_k++) {
                if (warp_k + 1 < Ks) {
                    #pragma unroll
                    for (int i = 0; i < Mr; i++) { //Can be done since Mr == Nr
                        a_frag[(warp_k + 1) % 2][i] = as[threadIdx.y * Mr + i][warp_k + 1];
                        b_frag[(warp_k + 1) % 2][i] = bs[warp_k + 1][threadIdx.x * Nr + i];
                    }
                }

                #pragma unroll
                for (int i = 0; i < Mr / 4; i++) {
                    #pragma unroll
                    for (int j = 0; j < Nr; j++) {
                        c_accumulator[i * 4][j] += a_frag[warp_k % 2][i * 4] * b_frag[warp_k % 2][j];
                        c_accumulator[i * 4 + 1][j] += a_frag[warp_k % 2][i * 4 + 1] * b_frag[warp_k % 2][j];
                        c_accumulator[i * 4 + 2][j] += a_frag[warp_k % 2][i * 4 + 2] * b_frag[warp_k % 2][j];
                        c_accumulator[i * 4 + 3][j] += a_frag[warp_k % 2][i * 4 + 3] * b_frag[warp_k % 2][j];
                    }
                }
            }

            //--------------------BARRIER---------------------
            __syncthreads();

            if (block_k + 1 < N / (2 * Ks)) {
                #pragma unroll
                for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                    as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] = a_next[i];
                    bs[threadIdx.y * thread_offset_short][threadIdx.x * thread_offset_long + i] = b_next[i];
                }
            }

            //--------------------BARRIER---------------------
            __syncthreads();
        }

        #pragma unroll
        for (int i = 0; i < Mr; i++) {
            #pragma unroll
            for (int j = 0; j < Nr; j++) {
                if(is_dual) {
                    atomicAdd(c + source_start_1 + block_offset_a + block_offset_b + thread_offset_c + i * N + j,
                              c_accumulator[i][j]
                    );
                    atomicAdd(c + source_start_2 + block_offset_a + block_offset_b + thread_offset_c + i * N + j,
                              c_accumulator[i][j]
                    );
                }
                else {
                    atomicAdd(c + source_start_1 + block_offset_a + block_offset_b + thread_offset_c + i * N + j,
                              c_accumulator[i][j]
                    );
                }
            }
        }
    }
}

__global__ void add_kernel(double *a, double *b, unsigned int N, bool isAdd) {
    unsigned int gid_x = threadIdx.x + blockIdx.x * blockDim.x;
    unsigned int gid_y = threadIdx.y + blockIdx.y * blockDim.y;
    unsigned int tot_x = gridDim.x * blockDim.x;
    unsigned int tot_y = gridDim.y * blockDim.y;
    unsigned int thread_offset_long = Ms / blockDim.y;
    unsigned int thread_offset_short = Ks / blockDim.x;

    unsigned int zero_start = 0;
    unsigned int one_start = N / 2;
    unsigned int two_start = (N * N) / 2;
    unsigned int three_start = (N * N + N) / 2;

    __shared__ double as[Ms][Ks];
    __shared__ double bs[Ks][Ns];

    //Define registers.
    double a_frag[2][Mr]; //While one vector of length Mr is being processed the other Mr vector prefetches.

    double *add_source_1 = a, *add_source_2 = b;
    unsigned int start_1, start_2, source_start;
    bool is_add;
    unsigned int block_local_x, block_local_y, block_offset, thread_offset;
    if(gid_x < tot_x / 2 && gid_y < tot_y / 2) {
        //Group 1
        block_local_x = blockIdx.x;
        block_local_y = blockIdx.y;

        //Addition. a0 + b0 -> a0
        start_1 = zero_start;
        start_2 = zero_start;
        source_start = zero_start;
        is_add = isAdd;
    }
    else if(gid_x >= tot_x / 2 && gid_y < tot_y / 2) {
        //Group 2
        block_local_x = blockIdx.x - gridDim.x / 2;
        block_local_y = blockIdx.y;

        //Subtraction. a1 + b1 -> a1
        start_1 = one_start;
        start_2 = one_start;
        source_start = one_start;
        is_add = isAdd;
    }
    else if(gid_x < tot_x / 2 && gid_y >= tot_y / 2) {
        //Group 3
        block_local_x = blockIdx.x;
        block_local_y = blockIdx.y - gridDim.y / 2;

        //Subtraction. a2 + b2 -> a2
        start_1 = two_start;
        start_2 = two_start;
        source_start = two_start;
        is_add = isAdd;
    }
    else if(gid_x >= tot_x / 2 && gid_y >= tot_y / 2) {
        //Group 4
        block_local_x = blockIdx.x - gridDim.x / 2;
        block_local_y = blockIdx.y - gridDim.y / 2;

        //Addition. a3 + b3 -> a3
        start_1 = three_start;
        start_2 = three_start;
        source_start = three_start;
        is_add = isAdd;
    }
    else {
        printf("gid_x = %d, gid_y = %d out of bounds\n", gid_x, gid_y);
    }

    block_offset = block_local_y * N * Ms + block_local_x * Ns;
    thread_offset = threadIdx.y * thread_offset_long * N + threadIdx.x * thread_offset_short;

    int block_k = 0;
    int iteration_offset = block_k * Ks;
    #pragma unroll
    for(int i = 0; i < thread_offset_long * thread_offset_short; i++) {
        as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] =
                add_source_1[start_1 + block_offset + iteration_offset +thread_offset + i * N];
        bs[threadIdx.x * thread_offset_short][threadIdx.y * thread_offset_long + i] =
                add_source_2[start_2 + block_offset + iteration_offset + thread_offset + i * N];
    }

    if(is_add) {
        #pragma unroll
        for (block_k = 0; block_k < Ns / Ks; block_k++) {
            iteration_offset = (block_k + 1) * Ks;
            #pragma unroll
            for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                a_frag[0][i] = as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] +
                               bs[threadIdx.x * thread_offset_short][threadIdx.y * thread_offset_long + i];

                if (block_k + 1 < Ns / Ks) {
                    as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] =
                            add_source_1[start_1 + block_offset + iteration_offset + thread_offset + i * N];
                    bs[threadIdx.x * thread_offset_short][threadIdx.y * thread_offset_long + i] =
                            add_source_2[start_2 + block_offset + iteration_offset + thread_offset + i * N];
                }
            }

            //Push values back to a
            iteration_offset = block_k * Ks;
            #pragma unroll
            for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                add_source_1[source_start + block_offset + iteration_offset + thread_offset + i * N] = a_frag[0][i];
            }
        }
    }
    else {
        #pragma unroll
        for (block_k = 0; block_k < Ns / Ks; block_k++) {
            iteration_offset = (block_k + 1) * Ks;
            #pragma unroll
            for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                a_frag[0][i] = as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] -
                               bs[threadIdx.x * thread_offset_short][threadIdx.y * thread_offset_long + i];

                if (block_k + 1 < Ns / Ks) {
                    as[threadIdx.y * thread_offset_long + i][threadIdx.x * thread_offset_short] =
                            add_source_1[start_1 + block_offset + iteration_offset + thread_offset + i * N];
                    bs[threadIdx.x * thread_offset_short][threadIdx.y * thread_offset_long + i] =
                            add_source_2[start_2 + block_offset + iteration_offset + thread_offset + i * N];
                }
            }

            //Push values back to b
            iteration_offset = block_k * Ks;
            #pragma unroll
            for (int i = 0; i < thread_offset_long * thread_offset_short; i++) {
                add_source_1[source_start + block_offset + iteration_offset + thread_offset + i * N] = a_frag[0][i];
            }
        }
    }
}

int main() {

    //Create A and B matrices.


    int test = 110;
    for(int i = test; i < test + 1; i++) {
        unsigned int N = 128 * i;

        double *c_cublas, *c_strassen, *d_a, *d_b, *d_c, *d_a1, *d_b1, *d_c1, *d_a2, *d_b2, *d_c2;
        cudaEvent_t start, end;
        cudaError_t error;
        float cublas_time = 0, strassen_time = 0, dgemm_time = 0;

        //Create pinned memory on host as its essential for async mem transfer.
        unsigned long long mem_size = sizeof(double) * N * N;
        gpuErrchk(cudaMallocHost((void **) &c_cublas, mem_size));
        gpuErrchk(cudaMallocHost((void **) &c_strassen, mem_size));

        cudaEventCreate(&start);
        cudaEventCreate(&end);

        dim3 grid(N * N / 16);
        dim3 block(4 * 4);

        //-------------------------CUBLAS-DGEMM------------------------------------


        gpuErrchk(cudaMalloc((void **) &d_a2, sizeof(double) * N * N))
        gpuErrchk(cudaMalloc((void **) &d_b2, sizeof(double) * N * N));
        gpuErrchk(cudaMalloc((void **) &d_c2, sizeof(double) * N * N));

        matrix_gen<<<grid, block>>>(d_a2, N, false);
        matrix_gen<<<grid, block>>>(d_b2, N, false);
        matrix_gen<<<grid, block>>>(d_c2, N, true);
        gpuErrchk(cudaPeekAtLastError());

        cublasStatus_t cStat;
        cublasHandle_t handle;
        const double alpha = 1, beta = 0;
        cStat = cublasCreate(&handle);
//        cStat = cublasSetMathMode(handle, CUBLAS_PEDANTIC_MATH);

        if(cStat != CUBLAS_STATUS_SUCCESS) {
            printf("\nFailed to initialize cublas handle!\n");
        }

        gpuErrchk(cudaEventRecord(start));
//        cStat = cublasDgemm_v2(handle,
//                       CUBLAS_OP_T, CUBLAS_OP_T,
//                       N, N, N,
//                       &alpha,
//                       d_a2, N,
//                       d_b2, N,
//                       &beta,
//                       d_c2, N
//                       );

        cStat = cublasGemmEx(handle,
                             CUBLAS_OP_T, CUBLAS_OP_T,
                             N, N, N,
                             &alpha,
                             d_a2, CUDA_R_64F, N,
                             d_b2, CUDA_R_64F, N,
                             &beta,
                             d_c2, CUDA_R_64F, N,
                             CUBLAS_COMPUTE_64F_PEDANTIC,
                             CUBLAS_GEMM_DEFAULT
        );

        if(cStat != CUBLAS_STATUS_SUCCESS) {
            printf("\nError performing dgemm: %d\n", cStat);
        }
        gpuErrchk(cudaEventRecord(end));
        gpuErrchk(cudaEventSynchronize(end));
        gpuErrchk(cudaEventElapsedTime(&dgemm_time, start, end));

        cublasDestroy_v2(handle);
        cudaFree(d_a2);
        cudaFree(d_b2);
        cudaFree(d_c2);

        //-------------------------CUBLAS-DGEMM-END------------------------------------

        //------------------------STRASSEN----------------------------------------
        gpuErrchk(cudaMalloc((void **) &d_a, sizeof(double) * N * N))
        gpuErrchk(cudaMalloc((void **) &d_b, sizeof(double) * N * N));
        gpuErrchk(cudaMalloc((void **) &d_c, sizeof(double) * N * N));

        matrix_gen<<<grid, block>>>(d_a, N, false);
        matrix_gen<<<grid, block>>>(d_b, N, false);
        matrix_gen<<<grid, block>>>(d_c, N, true);

        dim3 grid2_strassen(N / Ms, N / Ns);
        dim3 block2_strassen(8, 8);

        gpuErrchk(cudaFuncSetCacheConfig(&strassen_cutlass_stage1, cudaFuncCachePreferL1));
        gpuErrchk(cudaFuncSetCacheConfig(&strassen_cutlass_stage2, cudaFuncCachePreferL1));
        gpuErrchk(cudaFuncSetCacheConfig(&strassen_cutlass_stage3, cudaFuncCachePreferL1));
        gpuErrchk(cudaFuncSetCacheConfig(&strassen_cutlass_stage4, cudaFuncCachePreferL1));
        gpuErrchk(cudaFuncSetCacheConfig(&strassen_cutlass_stage5, cudaFuncCachePreferL1));
        gpuErrchk(cudaEventRecord(start));
        strassen_cutlass_stage1<<<grid2_strassen, block2_strassen>>>(d_a, d_b, N);
        strassen_cutlass_stage2<<<grid2_strassen, block2_strassen>>>(d_a, d_b, d_c, N);
        strassen_cutlass_stage3<<<grid2_strassen, block2_strassen>>>(d_a, d_b, N);
        strassen_cutlass_stage4<<<grid2_strassen, block2_strassen>>>(d_a, d_b, N);
        strassen_cutlass_stage5<<<grid2_strassen, block2_strassen>>>(d_a, d_b, d_c, N);
        gpuErrchk(cudaPeekAtLastError());
        gpuErrchk(cudaEventRecord(end));
        gpuErrchk(cudaEventSynchronize(end));
        gpuErrchk(cudaEventElapsedTime(&strassen_time, start, end));

        gpuErrchk(cudaMemcpy(c_strassen, d_c, sizeof(double) * N * N, cudaMemcpyDeviceToHost));

        cudaFreeHost(c_strassen);
        cudaFree(d_a);
        cudaFree(d_b);
        cudaFree(d_c);
        //------------------------STRASSEN-END-----------------------------------

        //---------------CUTLASS---------------------------------------------------

//        gpuErrchk(cudaMalloc((void **) &d_a1, sizeof(double) * N * N))
//        gpuErrchk(cudaMalloc((void **) &d_b1, sizeof(double) * N * N));
//        gpuErrchk(cudaMalloc((void **) &d_c1, sizeof(double) * N * N));
//
//        dim3 grid2(N / Ms, N / Ns);
//        dim3 block2(8, 8);
//
//        matrix_gen<<<grid, block>>>(d_a1, N, false);
//        matrix_gen<<<grid, block>>>(d_b1, N, false);
//        matrix_gen<<<grid, block>>>(d_c1, N, true);
//        gpuErrchk(cudaPeekAtLastError());
//
//
////        printf("\nStarting cutlass\n");
//        gpuErrchk(cudaEventRecord(start));
//        cublas_cutlass<<<grid2, block2>>>(d_a1, d_b1, d_c1, N);
//        gpuErrchk(cudaPeekAtLastError());
//        gpuErrchk(cudaEventRecord(end));
//        gpuErrchk(cudaEventSynchronize(end));
//        gpuErrchk(cudaEventElapsedTime(&cublas_time, start, end));
//
//        gpuErrchk(cudaMemcpy(c_cublas, d_c1, sizeof(double) * N * N, cudaMemcpyDeviceToHost));
//
        cudaFreeHost(c_cublas);
//        cudaFree(d_a1);
//        cudaFree(d_b1);
//        cudaFree(d_c1);
        //-------------------------CUTLASS-END-------------------------------------



        printf("\nAt %d elements -- Strassen Time: %f <-> DGEMM Time: %f : resulting in a gain of %f percent", N, strassen_time, dgemm_time,
               ((dgemm_time - strassen_time) * 100) / dgemm_time
               );
//        printf("\nAt %d elements -- Strassen Time: %f", N, strassen_time);
    }

    return 0;
}

bool checkMatrix(const double *a, const double *b, int N) {
    for(int i = 0; i < N; i++) {
        for(int j = 0; j < N; j++) {
            if(a[i * N + j] != b[i * N + j]) {
                return false;
            }
        }
    }

    return true;

}

bool validate(const double *a, const double *b, const double *c, int N) {
    for(int i = 0; i < N; i++) {
        for(int j = 0; j < N; j++) {
            double calc = 0;
            double A, B;
            for(int k = 0; k < N; k++) {
                A = a[i * N + k];
                B = b [k * N + j];
                calc += A * B;
            }

            if(calc != c[i * N + j]) {
                printf("\nError in c[%d][%d] GPU val = %f vs CPU cal = %f\n", i, j, c[i * N + j], calc);
                return false;
            }
        }
    }

    return true;
}

void printMat(double *a, unsigned int N, int stride, int start, char *filename) {
//    printf("\nPrinting to file started");
//    FILE *fp;
//
//    fopen_s(&fp, filename, "w+");
//
//    unsigned int length = N - stride;
//
//    for(unsigned int i = start; i < start + N * length; i += N) {
//        for(int j = 0; j < length; j++) {
//            fprintf(fp, "%f, ", a[i + j]);
//        }
//        fprintf(fp, "\n");
//    }
//
//    fclose(fp);
//    printf("\nPrinting completed");
}

float adderWrapper(double *a, double *b, unsigned int N, unsigned int n, bool isAdd=true) {
    double *d_a, *d_b;
    cudaEvent_t start, end;
    cudaEventCreate(&start);
    cudaEventCreate(&end);

    float strassen_time = 0, temp_time = 0;

    dim3 grid(localNAdd / Ms, localNAdd / Ns);
    dim3 block(8, 8);
    gpuErrchk(cudaMalloc((void**) &d_a, localNAdd * localNAdd * sizeof(double)));
    gpuErrchk(cudaMalloc((void**) &d_b, localNAdd * localNAdd * sizeof(double)));
    for(int i = 0; i < n; i += localNAdd) {
        for(int j = 0; j < n; j += localNAdd) {
            gpuErrchk(cudaMemcpy2D(d_a,
                                   localNAdd * sizeof(double),
                                   &a[i * N + j],
                                   N * sizeof(double),
                                   localNAdd * sizeof(double),
                                   localNAdd,
                                   cudaMemcpyHostToDevice
            ));
            gpuErrchk(cudaMemcpy2D(d_b,
                                   localNAdd * sizeof(double),
                                   &b[i * N + j],
                                   N * sizeof(double),
                                   localNAdd * sizeof(double),
                                   localNAdd,
                                   cudaMemcpyHostToDevice
            ));

            gpuErrchk(cudaEventRecord(start));
            add_kernel<<<grid, block>>>(d_a, d_b, localNAdd, isAdd);
            gpuErrchk(cudaPeekAtLastError());
            gpuErrchk(cudaEventRecord(end));
            gpuErrchk(cudaEventSynchronize(end));
            gpuErrchk(cudaEventElapsedTime(&temp_time, start, end));
            strassen_time += temp_time;
            gpuErrchk(cudaMemcpy2D(&a[i * N + j],
                                   N * sizeof(double),
                                   d_a,
                                   localNAdd * sizeof(double),
                                   localNAdd * sizeof(double),
                                   localNAdd,
                                   cudaMemcpyDeviceToHost
            ));
        }
    }

    cudaFree(d_a);
    cudaFree(d_b);

    return strassen_time;
}

float local_strassen_wrapper(double *a, double *b, double *c, double *c2, unsigned int N, unsigned int stride=0, bool isAdd=true, bool isDual=false, bool isAdd2=true) {
    //stride * stride is size of the local A, B and C matrix.
    if(stride == 0) {
        stride = N;
    }
    double *d_a, *d_b, *d_c;
    cudaEvent_t start, end;
    cudaEventCreate(&start);
    cudaEventCreate(&end);

    float strassen_time = 0, temp_time = 0;

    unsigned int memSizeDevice = sizeof(double) * stride * stride;

    unsigned int memSizeHostCol = sizeof(double) * N;
    unsigned int memSizeDeviceCol = sizeof(double) * stride;

    gpuErrchk(cudaMalloc((void **) &d_a, memSizeDevice));
    gpuErrchk(cudaMalloc((void **) &d_b, memSizeDevice));
    gpuErrchk(cudaMalloc((void **) &d_c, memSizeDevice));

    gpuErrchk(cudaMemcpy2D(d_a, memSizeDeviceCol, a, memSizeHostCol, memSizeDeviceCol, stride, cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpy2D(d_b, memSizeDeviceCol, b, memSizeHostCol, memSizeDeviceCol, stride, cudaMemcpyHostToDevice));

    //Initialize d_c to zero.
    dim3 grid(stride * stride / 16);
    dim3 block(4 * 4);
    matrix_gen<<<grid, block>>>(d_c, stride, true);


    dim3 grid2_strassen(stride / Ms, stride / Ns);
    dim3 block2_strassen(8, 8);

    gpuErrchk(cudaEventRecord(start));
    strassen_cutlass_stage1<<<grid2_strassen, block2_strassen>>>(d_a, d_b, stride);
    strassen_cutlass_stage2<<<grid2_strassen, block2_strassen>>>(d_a, d_b, d_c, stride);
    strassen_cutlass_stage3<<<grid2_strassen, block2_strassen>>>(d_a, d_b, stride);
    strassen_cutlass_stage4<<<grid2_strassen, block2_strassen>>>(d_a, d_b, stride);
    strassen_cutlass_stage5<<<grid2_strassen, block2_strassen>>>(d_a, d_b, d_c, stride);
    gpuErrchk(cudaPeekAtLastError());
    gpuErrchk(cudaEventRecord(end));
    gpuErrchk(cudaEventSynchronize(end));
    gpuErrchk(cudaEventElapsedTime(&temp_time, start, end));
    strassen_time += temp_time;

    //Clear d_b. Reuse d_a for c.
    cudaFree(d_b);
    gpuErrchk(cudaMemcpy2D(d_a, memSizeDeviceCol, c, memSizeHostCol, memSizeDeviceCol, stride, cudaMemcpyHostToDevice));

    gpuErrchk(cudaEventRecord(start));
    add_kernel<<<grid2_strassen, block2_strassen>>>(d_a, d_c, stride, isAdd);
    gpuErrchk(cudaPeekAtLastError());
    gpuErrchk(cudaEventRecord(end));
    gpuErrchk(cudaEventSynchronize(end));
    gpuErrchk(cudaEventElapsedTime(&temp_time, start, end));
    strassen_time += temp_time;
    gpuErrchk(cudaMemcpy2D(c, memSizeHostCol, d_a, memSizeDeviceCol, memSizeDeviceCol, stride, cudaMemcpyDeviceToHost));

    if(isDual) {
        gpuErrchk(cudaMemcpy2D(d_a, memSizeDeviceCol, c2, memSizeHostCol, memSizeDeviceCol, stride, cudaMemcpyHostToDevice));

        gpuErrchk(cudaEventRecord(start));
        add_kernel<<<grid2_strassen, block2_strassen>>>(d_a, d_c, stride, isAdd2);
        gpuErrchk(cudaPeekAtLastError());
        gpuErrchk(cudaEventRecord(end));
        gpuErrchk(cudaEventSynchronize(end));
        gpuErrchk(cudaEventElapsedTime(&temp_time, start, end));
        strassen_time += temp_time;
        gpuErrchk(cudaMemcpy2D(c2, memSizeHostCol, d_a, memSizeDeviceCol, memSizeDeviceCol, stride, cudaMemcpyDeviceToHost));
    }

    cudaFree(d_c);
    cudaFree(d_a);

    return strassen_time;
}

float global_strassen_wrapper(double *a, double *b, double *c, double *c2, unsigned int N, unsigned int n, bool isAdd, bool isDual, bool isAdd2) {
    //a, b are input matrices. c and c2 (optional) are the matrices to which the result of a * b will be added.
    //N is dimension of original matrix and n is the size of the sub matrix we are dealing with.
    //isAdd determines if result of a * b will be added to c and isAdd2 determines the same for c2
    //isDual determines whether we need to consider fusing results of a * b to c2.

    if(n <= localNMul) {
        return local_strassen_wrapper(a, b, c, c2, N, n, isAdd, isDual, isAdd2);
    }
    else {
        float strassen_time = 0;
        unsigned int zero_start, one_start, two_start, three_start;
        zero_start = 0;
        one_start = n / 2;
        two_start = (N * n) / 2;
        three_start = (N * n + n) / 2;
        //Perform another level of strassen spilt.
        //A2 + A3 -> A2
        //B1 - B3 -> B1
        //B2 - B0 -> B2
        //A1 + A0 -> A1
        strassen_time += adderWrapper(&a[two_start], &a[three_start], N, n / 2, true);
        strassen_time += adderWrapper(&b[one_start], &b[three_start], N, n / 2, false);
        strassen_time += adderWrapper(&b[two_start], &b[zero_start], N, n / 2, false);
        strassen_time += adderWrapper(&a[one_start], &a[zero_start], N, n / 2, true);

        //M1 = (A2 + A3) * B0 -> C2+ and C3-
        //M2 = A0 * (B1 - B3) -> C1+ and C3+
        //M3 = A3 * (B2 - B0) -> C0+ and C2+
        //M4 = (A0 + A1) * B3 -> C1+ and C0-
        strassen_time += global_strassen_wrapper(&a[two_start], &b[zero_start], &c[two_start], &c[three_start], N, n/2,isAdd, true, !isAdd);
        strassen_time += global_strassen_wrapper(&a[zero_start], &b[one_start], &c[one_start], &c[three_start], N, n/2,isAdd, true, isAdd);
        strassen_time += global_strassen_wrapper(&a[three_start], &b[two_start], &c[zero_start], &c[two_start], N, n/2,isAdd, true, isAdd);
        strassen_time += global_strassen_wrapper(&a[one_start], &b[three_start], &c[one_start], &c[zero_start], N, n/2,isAdd, true, !isAdd);

        if(isDual) {
            strassen_time += global_strassen_wrapper(&a[two_start], &b[zero_start], &c2[two_start], &c2[three_start], N, n/2,isAdd2, true, !isAdd2);
            strassen_time += global_strassen_wrapper(&a[zero_start], &b[one_start], &c2[one_start], &c2[three_start], N, n/2,isAdd2, true, isAdd2);
            strassen_time += global_strassen_wrapper(&a[three_start], &b[two_start], &c2[zero_start], &c2[two_start], N, n/2,isAdd2, true, isAdd2);
            strassen_time += global_strassen_wrapper(&a[one_start], &b[three_start], &c2[one_start], &c2[zero_start], N, n/2,isAdd2, true, !isAdd2);
        }

        //A0 + A3 -> A0
        //B0 + B3 -> B0
        //A2 - A0 -> A2
        //A1 - A0 -> A1
        //B1 + B0 -> B1
        //B2 + B0 -> B2
        strassen_time += adderWrapper(&a[zero_start], &a[three_start], N, n / 2, true);
        strassen_time += adderWrapper(&b[zero_start], &b[three_start], N, n / 2, true);
        strassen_time += adderWrapper(&a[two_start], &a[zero_start], N, n / 2, false);
        strassen_time += adderWrapper(&a[one_start], &a[zero_start], N, n / 2, false);
        strassen_time += adderWrapper(&b[one_start], &b[zero_start], N, n / 2, true);
        strassen_time += adderWrapper(&b[two_start], &b[zero_start], N, n / 2, true);

        //M0 = (A0 + A3) * (B0 + B3) -> C0+ and C3+
        //M5 = (A2 - A0) * (B1 + B0) -> C3+
        //M6 = (A1 - A3) * (B2 + B3) -> C0+
        strassen_time += global_strassen_wrapper(&a[zero_start], &b[zero_start], &c[zero_start], &c[three_start], N, n/2,isAdd, true, isAdd);
        strassen_time += global_strassen_wrapper(&a[two_start], &b[one_start], &c[three_start], &c[three_start], N, n/2,isAdd, false, isAdd);
        strassen_time += global_strassen_wrapper(&a[one_start], &b[two_start], &c[zero_start], &c[zero_start], N, n/2,isAdd, false, isAdd);

        if(isDual) {
            strassen_time += global_strassen_wrapper(&a[zero_start], &b[zero_start], &c2[zero_start], &c2[three_start], N, n/2,isAdd2, true, isAdd2);
            strassen_time += global_strassen_wrapper(&a[two_start], &b[one_start], &c2[three_start], &c2[three_start], N, n/2,isAdd2, false, isAdd2);
            strassen_time += global_strassen_wrapper(&a[one_start], &b[two_start], &c2[zero_start], &c2[zero_start], N, n/2,isAdd2, false, isAdd2);
        }

        //Steps to recreate A0, A1, A2, B0, B1, B2 back to its initial values. Essentially reconstruct the butchered A and B sub matrices we used as interim storage.
        strassen_time += adderWrapper(&a[zero_start], &a[three_start], N, n / 2, false);
        strassen_time += adderWrapper(&a[one_start], &a[three_start], N, n / 2, true);
        strassen_time += adderWrapper(&a[two_start], &a[zero_start], N, n / 2, true);

        strassen_time += adderWrapper(&b[zero_start], &b[three_start], N, n / 2, false);
        strassen_time += adderWrapper(&b[two_start], &b[three_start], N, n / 2, false);
        strassen_time += adderWrapper(&b[one_start], &b[zero_start], N, n / 2, false);

        return strassen_time;
    }
}

//TODO localNMUl localNAdd